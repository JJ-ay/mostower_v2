# MOS Tower v2

This repository stores the code for [FRDM-K64F Kinetis](https://www.nxp.com/design/development-boards/freedom-development-boards/mcu-boards/freedom-development-platform-for-kinetis-k64-k63-and-k24-mcus:FRDM-K64F) board controlling MOS Tower - an interactive model of Wrocław's tallest building Sky Tower equipped with a 20x30 LED matrix.

![](https://gitlab.com/JJ-ay/mostower_v2/-/raw/master/media/tower_intro.webm)

## About the Project

The project's been originally written by me for Microsystems Oriented Society students club at Wrocław University of Sicence and Technology but I decided to move it to a private repo.

Project allows user to display in real time animations encoded in `.piwo7` format (MOS internal animation format) at 20FPS. In original implementation it was supposed to work toghether with PIWO Player software via Ethernet connection which was sending the animation data. There was also a separate program for creating animations. In current form the project's waiting for me to write a simple animation player (and editor).

More detailed documentation (in polish) can be found in [doc/MOSTower_Manual.pdf](https://gitlab.com/JJ-ay/mostower_v2/-/blob/master/doc/MOSTower_Manual.pdf)

## Startup Message

As seen in the video from [Introduction section](#introduction) model displays a welcoming message on powerup. It's possible to provide your own message by following these steps:

1. Make sure your desired animation has 20x30 resolution.
1. Remove `source/welcome.h` file.
1. Use `scripts/piwo7_to_tower.py` script to convert `.piwo7` animation to a C header file with defined animation.
1. Place generated file at `source/welcome.h` path.

## Notes 

Currently used `CmakeLists.txt` and `toolchain.cmake` files were parts of example code from NXP (edited by me) and I think the are absolutely *hideous*. I tried my best to tame and build LWiP but linker defeated me eventually.

## Example animation

![](https://gitlab.com/JJ-ay/mostower_v2/-/raw/master/media/tower_rick_n_morty.webm)

