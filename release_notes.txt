v2.3
- Poprawiona animacja startowa
- Dodany skrypt do konwersji *.piwo7 na *.h z animacją
- Dodana animacja powitalna do folderu doc

v2.2
- Dodana animacja startowa
- W następnej wersji skrypt do konwersji *.piwo7 na const tablicę do zaflashowania

v2.1
- Hotfix wyświetlania (błąd w merge-u)
- Poprawka w manualu

v2.0
- Reaktywacja Towera
