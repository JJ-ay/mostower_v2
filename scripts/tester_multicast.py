#!/usr/bin/env python

##############################################################
# Before you can use this, you need to connect to multicast: #
# ip route add 239.255.43.21 dev <net_dev_name>              #
#                                                            #
# To disconnect from multicast use:                          #
# ip route del 239.255.43.21 dev <net_dev_name>              #
# (Disconnecting ethernet cable also works, lol)             #
##############################################################

import socket
import struct
# import time

T_COL = 20
T_ROW = 30

MCAST_GRP = '239.255.43.21'
MCAST_PORT = 6667
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)

sock.bind(('', MCAST_PORT))

mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

MESSAGE = bytearray([T_COL, T_ROW]) + T_COL*T_ROW*bytearray([0xFF, 0x00, 0x00])

## TESTER POJEDYNCZEJ KOLUMNY
#TEST_COLUMN = 0
#for j in range(T_ROW):
#    MESSAGE[(2+3*(T_COL*j + TEST_COLUMN)+0)] = 255
#    MESSAGE[(2+3*(T_COL*j + TEST_COLUMN)+1)] = 255
#    MESSAGE[(2+3*(T_COL*j + TEST_COLUMN)+2)] = 255
#print(MESSAGE)

sock.sendto(MESSAGE, (MCAST_GRP, MCAST_PORT))

# j = 0

# while(True):
#     MESSAGE = bytearray([T_COL, T_ROW]) + T_COL*T_ROW*bytearray([0xFF, 0x00, 0x00])
#     sock.sendto(MESSAGE, (MCAST_GRP, MCAST_PORT))
#     time.sleep(2.5)
#     MESSAGE = bytearray([T_COL, T_ROW]) + T_COL*T_ROW*bytearray([0x00, 0xFF, 0x00])
#     sock.sendto(MESSAGE, (MCAST_GRP, MCAST_PORT))
#     time.sleep(2.5)
#     MESSAGE = bytearray([T_COL, T_ROW]) + T_COL*T_ROW*bytearray([0x00, 0x00, 0xFF])
#     sock.sendto(MESSAGE, (MCAST_GRP, MCAST_PORT))
#     time.sleep(2.5)

# while(True):
#     print("Tap to send Packed id: " + str(j))
#     x = input()
#     sock.sendto(MESSAGE, (MCAST_GRP, MCAST_PORT))
#     j += 1
#     time.sleep(0.001)
