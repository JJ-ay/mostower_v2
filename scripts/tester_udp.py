#!/usr/bin/env python

import socket
import time
UDP_IP = "192.168.0.6"
UDP_PORT = 6667
sock = socket.socket((socket.AF_INET), (socket.SOCK_DGRAM))
sock.settimeout(3)

# MESSAGE = bytearray([0xBB])
MESSAGE = bytes("123456789", 'ascii')

print(MESSAGE)

j = 0

while(True):
    print("Tap to send Packed id: " + str(j))
    x = input()
    print(MESSAGE)
    sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
    ans = sock.recvfrom(1000)
    print(ans)
    j += 1
    time.sleep(0.001)
