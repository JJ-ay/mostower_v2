
#include "main.h"

/*!
 * @brief Main function
 */
extern void main_cpp(void);

int main(void)
{
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    BOARD_InitLEDsPins();
    /* Disable SYSMPU. */
    SYSMPU->CESR &= ~SYSMPU_CESR_VLD_MASK;

    LED_GREEN_ON();     // Indicator for ongoing startup; turns off after establishing ethernet connection

    main_cpp();

    while(1){}

    return 0;
}
