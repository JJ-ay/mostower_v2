
#include "tower.h"
#include "MK64F12.h"
#include "fsl_common.h"
#include "fsl_edma.h"
#include "fsl_ftm.h"
#include "fsl_gpio.h"
#include <sys/_stdint.h>

static bool welcome = 0;

void EDMA_Callback(edma_handle_t *handle, void *param, bool transferDone, uint32_t tcds)
{
    if (transferDone)
    {
        dma_finished = true;
        FTM0->SC &= ~(FTM_SC_CLKS_MASK); // stop DMA triggering timer 
        __DSB();
    }
}

void vCallback_WELCOME_TIM(TimerHandle_t xTimer)
{
    /* Prompt TOWER_DisplayWelcome() to update picture */
    xTaskNotifyGive(tsk_welcome);
}

void TOWER_DisplayWelcome(void * frame_data)
{
    /* If for some reason the display ever had different resolution just don't show the welcome message */
#if((TOWER_LEN_COLUMNS == ANIM_WELCOME_COLS)&&(TOWER_LEN_ROWS == ANIM_WELCOME_ROWS)&&(TOWER_DISPLAY_WELCOME == 1))
    uint8_t * frame = (uint8_t *)frame_data;
    vTaskDelay(pdMS_TO_TICKS(1000));        // just user experience stuff
    welcome = 1;
    xTimerStart(tim_welcome,pdMS_TO_TICKS(500));

    for(int i=0; i<ANIM_WELCOME_FRAMES; i++)
    {
        for(int j=0; j<1800; j++)
            frame[j] = anim_welcome[i][j];
        ulTaskNotifyTake(pdTRUE, pdMS_TO_TICKS(100));    
        xTaskNotifyGive(tsk_unpack_piwo);
    }
    
    xTimerStop(tim_welcome,0);
    welcome = 0;
#endif
    xTaskNotifyGive(tsk_config_net);
    
    vTaskDelete(NULL);
    TSK_DIE(); // infinite delay just in case
} 

/*  Counts pins connected to a perticular port among A, B, C and D and stores indexes of proper pin masks
    in 2D array "port_array"
    For example : port_array[0][0] returns first pin connected to port A, port_array[2][2] returns third pin connected to the port C */
void TOWER_GroupingFunction(void)
{
    PORT_Type* port_type[4] = {PORTA, PORTB, PORTC, PORTD};
    uint32_t array_iter = 0;

    for(uint32_t port_type_iter=0; port_type_iter < 4; ++port_type_iter){
        //saves proper indexes
        for(uint32_t iter=0; iter < TOWER_LEN_COLUMNS; ++iter){
            if (tower_pinout.port[iter] == port_type[port_type_iter]){
                port_array.ports[port_type_iter][array_iter] = iter;
                ++array_iter;
            }
        }
        //saves size of each array
        port_array.port_size[port_type_iter] = array_iter+1;
        array_iter = 0;
    }
}

void TOWER_UnpackPIWO(void * frame_data)
{
    uint32_t index;
    uint32_t column_pscor;
    volatile struct window_t window;
    while(1)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);    // block task until new data arrives on enet
        uint8_t * frame;

        if(welcome)
            frame = (uint8_t *)(frame_data);
        else
            frame = (uint8_t *)(frame_data+2);   // skipping two first bytes

        /* Unpack data from frame to columns */
        for(int port=0; port<TOWER_PORT_COUNT; port++)
        {
            for(int column=0; column < port_array.port_size[port]; ++column)
            {
                ws2812_column_pdor[0] = 0U; // fixes display errors 
                column_pscor |= tower_pinout.pin_mask[port_array.ports[port][column]]; // 

                for(int row=0; row<TOWER_LEN_ROWS; row++)
                {
                    /* PIWO Player sends data by rows and we need to display them by columns */
                    window.colors.color.blue  = frame[ 3*(TOWER_LEN_COLUMNS*row + port_array.ports[port][column])+2 ];
                    window.colors.color.red   = frame[ 3*(TOWER_LEN_COLUMNS*row + port_array.ports[port][column])   ];
                    window.colors.color.green = frame[ 3*(TOWER_LEN_COLUMNS*row + port_array.ports[port][column])+1 ];
                    //po tym mamy kolory dla jednego leda(okienka)

                    /*ws2812_column_pdor - wartosci portu do wysłania do portu (0 lub 1), przechowuje dla całej jednej kolumny (jednego paska led)
                    wysyłamy 3 bajty każdy po 8 bitów
                    bit po bicie zmienia na bajty 
                    (window.colors.all & (1U<<k)) sprawdzenie czy dany wybrany bit to 1 czy 0
                    tower_pinout.pin_mask[col]*/

                    for(int k=0; k<24; k++)
                    {
                        index = 1+k+((TOWER_LEN_ROWS-1)-row)*24; // offset by 1 because first element is already set as 0 ...
                                                                 // ... also (len-1)-row because we need to flip the order of the windows
                        if(window.colors.all & (1U<<(23-k)))
                            ws2812_column_pdor[index] |= tower_pinout.pin_mask[port_array.ports[port][column]];
                        else
                            ws2812_column_pdor[index] &= ~(tower_pinout.pin_mask[port_array.ports[port][column]]);
                    }
                }
            }
            disp_gpio = tower_pinout.gpio[port_array.ports[port][0]];
            ws2812_column_pscor[0]=0U;

            for(int iter=1; iter<WS2812_BUFF_LEN; iter++)
                ws2812_column_pscor[iter] = column_pscor;

            xTaskNotifyGive(tsk_display_pic); // UnpackPIWO will wait in READY state until DisplayPicture finishes column display

            memset(ws2812_column_pdor, 0, sizeof(ws2812_column_pdor));
            column_pscor = 0;
        }
    }
}

void TOWER_DisplayPicture(void * arg)
{
    edma_transfer_config_t edma_trans_ch0;
    edma_transfer_config_t edma_trans_ch1;
    edma_transfer_config_t edma_trans_ch2;

    while(1)
    {
        ulTaskNotifyTake(pdTRUE,portMAX_DELAY);

        EDMA_PrepareTransfer(&edma_trans_ch0, (void*)&ws2812_column_pscor, sizeof(uint32_t), (void*)&disp_gpio->PSOR, sizeof(uint32_t),
                sizeof(uint32_t), WS2812_BUFF_LEN*sizeof(uint32_t), kEDMA_MemoryToPeripheral);
        EDMA_PrepareTransfer(&edma_trans_ch1, (void*)&ws2812_column_pdor, sizeof(uint32_t), (void*)&disp_gpio->PDOR, sizeof(uint32_t),
                sizeof(uint32_t), WS2812_BUFF_LEN*sizeof(uint32_t), kEDMA_MemoryToPeripheral);
        EDMA_PrepareTransfer(&edma_trans_ch2, (void*)&ws2812_column_pscor, sizeof(uint32_t), (void*)&disp_gpio->PCOR, sizeof(uint32_t),
                sizeof(uint32_t), WS2812_BUFF_LEN*sizeof(uint32_t), kEDMA_MemoryToPeripheral);

        /* We need to disable address incrementing for everything despite ws2812_column */
        edma_trans_ch0.destOffset = 0;
        edma_trans_ch1.destOffset = 0;
        edma_trans_ch2.destOffset = 0;

        EDMA_SubmitTransfer(&dma_ch0_hnld, &edma_trans_ch0);
        EDMA_SubmitTransfer(&dma_ch1_hnld, &edma_trans_ch1);
        EDMA_SubmitTransfer(&dma_ch2_hnld, &edma_trans_ch2);

        TOWER_SetLedColumn();
    }
}

void TOWER_SetLedColumn()
{
    /* NOT A RTOS TASK */
    /* But needs to be called by one */

    xSemaphoreTake(mut_comm,portMAX_DELAY);

    dma_finished = false;
    FTM0->CNT = 0U; 

    EDMA_StartTransfer(&dma_ch0_hnld);
    EDMA_StartTransfer(&dma_ch1_hnld);
    EDMA_StartTransfer(&dma_ch2_hnld);
    FTM_StartTimer(FTM0,kFTM_SystemClock);

    while(!dma_finished){__NOP();}     

    // TODO to be sure we get 50 us of 0V to clear receive buffer in ws2812
    GPIOA->PDOR = 0U;
    GPIOB->PDOR = 0U;
    GPIOC->PDOR = 0U;
    GPIOD->PDOR = 0U; 

    xSemaphoreGive(mut_comm);
}

void TOWER_InitTimerLED(void)
{
    /* NOT A RTOS TASK */

    ftm_config_t ftm_conf;
    FTM_GetDefaultConfig(&ftm_conf);
    ftm_conf.prescale = TIMERS_PRESCALER;
    FTM_Init(FTM0, &ftm_conf);

    /* WS2812B */
    FTM0->CNTIN = 0;
    FTM_SetupOutputCompare(FTM0,kFTM_Chnl_0,kFTM_NoOutputSignal,21U);  
    FTM_SetupOutputCompare(FTM0,kFTM_Chnl_1,kFTM_NoOutputSignal,45U);  
    FTM_SetupOutputCompare(FTM0,kFTM_Chnl_2,kFTM_NoOutputSignal,69U);  
//    FTM_SetupOutputCompare(FTM0,kFTM_Chnl_0,kFTM_ToggleOnMatch,1U);  
//    FTM_SetupOutputCompare(FTM0,kFTM_Chnl_1,kFTM_ToggleOnMatch,25U);  
//    FTM_SetupOutputCompare(FTM0,kFTM_Chnl_2,kFTM_ToggleOnMatch,49U);  
    FTM_SetTimerPeriod(FTM0,74U);  

    //     WS2812B
    //     | 400 |    850    |    800    | 450 |    
    //
    //      _____             ___________       _ ...
    //     |     |           |           |     | 
    //... _|     |___________|           |_____|
    //
    //     |--- WS2812B 0 ---|--- WS2812B 1 ---|
    //0-pscor, 400 - psdor, 800 - pscor

    FTM_EnableInterrupts(FTM0,kFTM_Chnl0InterruptEnable); 
    FTM_EnableInterrupts(FTM0,kFTM_Chnl1InterruptEnable); 
    FTM_EnableInterrupts(FTM0,kFTM_Chnl2InterruptEnable); 

    FTM_EnableDmaTransfer(FTM0,kFTM_Chnl_0, true);
    FTM_EnableDmaTransfer(FTM0,kFTM_Chnl_1, true);
    FTM_EnableDmaTransfer(FTM0,kFTM_Chnl_2, true);
}

void TOWER_InitDmaLED(void)
{
    /* NOT A RTOS TASK */
    
    edma_config_t edma_usr_conf;
    EDMA_GetDefaultConfig(&edma_usr_conf);
    edma_usr_conf.enableHaltOnError = false;
    edma_usr_conf.enableRoundRobinArbitration = true;

    DMAMUX_Init(DMAMUX0);
    DMAMUX_SetSource(DMAMUX0, 0, 0x14);
    DMAMUX_SetSource(DMAMUX0, 1, 0x15);
    DMAMUX_SetSource(DMAMUX0, 2, 0x16);
    DMAMUX_EnableChannel(DMAMUX0, 0);
    DMAMUX_EnableChannel(DMAMUX0, 1);
    DMAMUX_EnableChannel(DMAMUX0, 2);

    EDMA_Init(DMA0, &edma_usr_conf);
    EDMA_CreateHandle(&dma_ch0_hnld, DMA0, 0);
    EDMA_CreateHandle(&dma_ch1_hnld, DMA0, 1);
    EDMA_CreateHandle(&dma_ch2_hnld, DMA0, 2);
    EDMA_SetCallback(&dma_ch2_hnld, EDMA_Callback, NULL);     
}

void TOWER_PinMux(void)
{
    /* NOT A RTOS TASK */
    
    for(int col = 0; col < TOWER_LEN_COLUMNS; col++)
    {
       PORT_SetPinConfig(tower_pinout.port[col], tower_pinout.pin[col], &tower_port_cfg);
       GPIO_PinInit(tower_pinout.gpio[col], tower_pinout.pin[col], &tower_pin_cfg);
    }
}
