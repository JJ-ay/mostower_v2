
#pragma once

#include "board.h"
#include "main.h"

#define DBG 0

/**************** Pins and ports definitions ****************/

#define TOWER_GPIO_0     GPIOB
#define TOWER_PORT_0     PORTB
#define TOWER_PIN_0      23UL

#define TOWER_GPIO_1     GPIOC
#define TOWER_PORT_1     PORTC
#define TOWER_PIN_1      2UL

#if DBG
#define TOWER_GPIO_2     TOWER_GPIO_0
#define TOWER_PORT_2     TOWER_PORT_0
#define TOWER_PIN_2      TOWER_PIN_0
#else
#define TOWER_GPIO_2     GPIOA
#define TOWER_PORT_2     PORTA
#define TOWER_PIN_2      2UL
#endif

#define TOWER_GPIO_3     GPIOC
#define TOWER_PORT_3     PORTC
#define TOWER_PIN_3      3UL

#define TOWER_GPIO_4     GPIOB
#define TOWER_PORT_4     PORTB
#define TOWER_PIN_4      2UL

#define TOWER_GPIO_5     GPIOC
#define TOWER_PORT_5     PORTC
#define TOWER_PIN_5      11UL

#define TOWER_GPIO_6     GPIOB
#define TOWER_PORT_6     PORTB
#define TOWER_PIN_6      10UL

#define TOWER_GPIO_7     GPIOB
#define TOWER_PORT_7     PORTB
#define TOWER_PIN_7      3UL

#define TOWER_GPIO_8     GPIOB
#define TOWER_PORT_8     PORTB
#define TOWER_PIN_8      11UL

#define TOWER_GPIO_9     GPIOC
#define TOWER_PORT_9     PORTC
#define TOWER_PIN_9      10UL

#define TOWER_GPIO_10    GPIOB
#define TOWER_PORT_10    PORTB
#define TOWER_PIN_10     18UL

#define TOWER_GPIO_11    GPIOB
#define TOWER_PORT_11    PORTB
#define TOWER_PIN_11     19UL

#define TOWER_GPIO_12    GPIOC
#define TOWER_PORT_12    PORTC
#define TOWER_PIN_12     8UL

#define TOWER_GPIO_13     GPIOC
#define TOWER_PORT_13     PORTC
#define TOWER_PIN_13      1UL

#define TOWER_GPIO_14     GPIOC
#define TOWER_PORT_14     PORTC
#define TOWER_PIN_14      5UL

#define TOWER_GPIO_15     GPIOC
#define TOWER_PORT_15     PORTC
#define TOWER_PIN_15      0UL

#define TOWER_GPIO_16     GPIOC
#define TOWER_PORT_16     PORTC
#define TOWER_PIN_16      9UL

#define TOWER_GPIO_17     GPIOC
#define TOWER_PORT_17     PORTC
#define TOWER_PIN_17      7UL

#if DBG
#define TOWER_GPIO_18     TOWER_GPIO_0
#define TOWER_PORT_18     TOWER_PORT_0
#define TOWER_PIN_18      TOWER_PIN_0
#else
#define TOWER_GPIO_18     GPIOA
#define TOWER_PORT_18     PORTA
#define TOWER_PIN_18      1UL
#endif

#define TOWER_GPIO_19     GPIOB
#define TOWER_PORT_19     PORTB
#define TOWER_PIN_19      9UL

/*************************************************************/

struct PortPin_t_{
   PORT_Type * port [TOWER_LEN_COLUMNS];
   GPIO_Type * gpio [TOWER_LEN_COLUMNS];
   uint8_t pin [TOWER_LEN_COLUMNS];
   uint32_t pin_mask [TOWER_LEN_COLUMNS];
};

port_pin_config_t tower_port_cfg = {  
   kPORT_PullDisable,
   kPORT_FastSlewRate,
   kPORT_PassiveFilterDisable,
   kPORT_OpenDrainDisable,
   kPORT_LowDriveStrength,
   kPORT_MuxAsGpio,
   kPORT_UnlockRegister
};

gpio_pin_config_t tower_pin_cfg = {
   kGPIO_DigitalOutput,
   0U
};

struct PortPin_t_ tower_pinout =
{
    // PORTS 
    {
        TOWER_PORT_0, 
        TOWER_PORT_1,
        TOWER_PORT_2, 
        TOWER_PORT_3, 
        TOWER_PORT_4, 
        TOWER_PORT_5, 
        TOWER_PORT_6, 
        TOWER_PORT_7, 
        TOWER_PORT_8, 
        TOWER_PORT_9, 
        TOWER_PORT_10,
        TOWER_PORT_11,
        TOWER_PORT_12,
        TOWER_PORT_13,
        TOWER_PORT_14,
        TOWER_PORT_15,
        TOWER_PORT_16,
        TOWER_PORT_17,
        TOWER_PORT_18,
        TOWER_PORT_19
    },
    // GPIOS
    {
        TOWER_GPIO_0, 
        TOWER_GPIO_1,
        TOWER_GPIO_2, 
        TOWER_GPIO_3, 
        TOWER_GPIO_4, 
        TOWER_GPIO_5, 
        TOWER_GPIO_6, 
        TOWER_GPIO_7, 
        TOWER_GPIO_8, 
        TOWER_GPIO_9, 
        TOWER_GPIO_10,
        TOWER_GPIO_11,
        TOWER_GPIO_12,
        TOWER_GPIO_13,
        TOWER_GPIO_14,
        TOWER_GPIO_15,
        TOWER_GPIO_16,
        TOWER_GPIO_17,
        TOWER_GPIO_18,
        TOWER_GPIO_19
    },
    // PINS
    {
        TOWER_PIN_0, 
        TOWER_PIN_1,
        TOWER_PIN_2, 
        TOWER_PIN_3, 
        TOWER_PIN_4, 
        TOWER_PIN_5, 
        TOWER_PIN_6, 
        TOWER_PIN_7, 
        TOWER_PIN_8, 
        TOWER_PIN_9, 
        TOWER_PIN_10,
        TOWER_PIN_11,
        TOWER_PIN_12,
        TOWER_PIN_13,
        TOWER_PIN_14,
        TOWER_PIN_15,
        TOWER_PIN_16,
        TOWER_PIN_17,
        TOWER_PIN_18,
        TOWER_PIN_19
    },
    // PIN MASKS
    {
        (uint32_t)(1UL << TOWER_PIN_0),
        (uint32_t)(1UL << TOWER_PIN_1),
        (uint32_t)(1UL << TOWER_PIN_2),
        (uint32_t)(1UL << TOWER_PIN_3),
        (uint32_t)(1UL << TOWER_PIN_4),
        (uint32_t)(1UL << TOWER_PIN_5),
        (uint32_t)(1UL << TOWER_PIN_6),
        (uint32_t)(1UL << TOWER_PIN_7),
        (uint32_t)(1UL << TOWER_PIN_8),
        (uint32_t)(1UL << TOWER_PIN_9),
        (uint32_t)(1UL << TOWER_PIN_10),
        (uint32_t)(1UL << TOWER_PIN_11),
        (uint32_t)(1UL << TOWER_PIN_12),
        (uint32_t)(1UL << TOWER_PIN_13),
        (uint32_t)(1UL << TOWER_PIN_14),
        (uint32_t)(1UL << TOWER_PIN_15),
        (uint32_t)(1UL << TOWER_PIN_16),
        (uint32_t)(1UL << TOWER_PIN_17),
        (uint32_t)(1UL << TOWER_PIN_18),
        (uint32_t)(1UL << TOWER_PIN_19)
    }
};
