

extern "C"{

#include "tower_main.h"
#include "main.h"

    void vCallback_LED_R_TIM(TimerHandle_t xTimer)
    {
        LED_RED_TOGGLE();
    }

    /*!
     * @brief Main function
     */
    void main_cpp(void)
    {
        TOWER_PinMux();
        TOWER_GroupingFunction();
        TOWER_InitDmaLED();
        TOWER_InitTimerLED();

        xTaskCreate(NETIF_Config, "conf_net", 256, NULL, PRIO_CONF_NET, &tsk_config_net);
        xTaskCreate(NETIF_RcvHandler, "read_udp",  500, (void*)frame_data, PRIO_READ_ETH, &tsk_rcv_eth);
        xTaskCreate(NETIF_HandleFrame,"frm_handl", 256, (void*)frame_data, PRIO_HANDLE_FRAME, &tsk_handle_frame);
        xTaskCreate(TOWER_UnpackPIWO, "unpck_frm", 256, (void*)frame_data, PRIO_UNPACK_FRAME, &tsk_unpack_piwo);
        xTaskCreate(TOWER_DisplayWelcome, "welcome", 256, (void*)frame_data, PRIO_WELCOME, &tsk_welcome);
        xTaskCreate(TOWER_DisplayPicture, "disp_pic", 500, NULL, PRIO_DISPL_PIC, &tsk_display_pic);

        tim_led_r = xTimerCreate("LED_B_TIM", pdMS_TO_TICKS(1000), pdTRUE, (void*)TIM_ID_LED_B, vCallback_LED_R_TIM);
        tim_welcome = xTimerCreate("WELCOME_TIM", pdMS_TO_TICKS(50), pdTRUE, (void*)TIM_ID_WELCOME, vCallback_WELCOME_TIM);

        /* Communication does not seem to work due to lwip conflicts while there are ongoing dma transfers. */
        /* EthHandler and DisplayPicture exchange mut_comm in order not to let conflicts appear. */
        mut_comm = xSemaphoreCreateMutex();

        vTaskStartScheduler();

        /* Will not get here unless a task calls vTaskEndScheduler ()*/
        while(1){}
    }

    void TSK_DIE()
    {
        while(1)
        {
            vTaskDelay(portMAX_DELAY);
        }
    }
} // extern "C"

