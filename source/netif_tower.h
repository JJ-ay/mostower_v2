
#pragma once

#include <string.h>
#include "main.h"
#include "tower_main.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "lwipopts.h"

#include "netifapi.h"
#include "tcpip.h"
#include "sys.h"
#include "api.h"
#include "ethernet.h"
#include "enet_ethernetif.h"

#ifdef __cplusplus
}
#endif

#include "board.h"
#include "fsl_enet_mdio.h"
#include "fsl_phyksz8081.h"

#define CLOCK_FREQ CLOCK_GetFreq(kCLOCK_CoreSysClk)
#define TOWER_BUF_LEN 2700

/* MAC address configuration. */
#define configMAC_ADDR                     \
    {                                      \
        0x01, 0x00, 0x5E, 0x69, 0x69, 0x69 \
    }

/* Netif handler */
struct netif netif;

/* Frame received from ETH */
volatile uint8_t frame_data[TOWER_BUF_LEN];                                               
volatile uint32_t frame_len;

/* Function configuring network interface parameters */
void NETIF_Config(void * arg);

/* Function to receive data through ETH */
void NETIF_RcvHandler(void * frame_buff);

/* Function handling received data */
void NETIF_HandleFrame(void * frame_data);

