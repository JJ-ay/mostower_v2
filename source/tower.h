
#pragma once

#include "main.h"
#include "tower_main.h"
#include "board.h"
#include "tower_pinout.h"
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "pin_mux.h"
#include "fsl_dmamux.h"
#include "fsl_edma.h"
#include "fsl_ftm.h"
#include "semphr.h"
#include "welcome.h"

#define TIMERS_PRESCALER kFTM_Prescale_Divide_1
#define TIMERS_CLOCK (uint64_t)(CLOCK_GetFreq(kCLOCK_BusClk)/1) // set division accordingly to prescaler
#define WS2812_BUFF_LEN (1+TOWER_LEN_ROWS*24U)  // 8 bit x 3 (RGB) per row + zeroes at the beginning

struct window_t
{
    union Colors_t
    {
        uint32_t all;
        struct Color_t
        {
            uint8_t blue;
            uint8_t red;
            uint8_t green;
            uint8_t dummy_byte;
        } __attribute__((packed,aligned(4))) color;
    } __attribute__((packed,aligned(4))) colors;
};

struct Ports_indexes_t
{
    uint32_t ports[4][TOWER_LEN_COLUMNS]; //4 arrays for ports (A,B,C,D) that store pin indexes
    uint8_t port_size[4];    
}port_array;

GPIO_Type * disp_gpio;

uint32_t ws2812_column_pdor [WS2812_BUFF_LEN];       // stores color data for leds (needs to start with zero)
uint32_t ws2812_column_pscor [WS2812_BUFF_LEN];      // stores current column mask (needs to start with zero)

volatile bool dma_finished = false;
edma_handle_t dma_ch0_hnld;
edma_handle_t dma_ch1_hnld;
edma_handle_t dma_ch2_hnld;

void vCallback_WELCOME_TIM(TimerHandle_t xTimer);

void TOWER_DisplayWelcome(void * arg);
void TOWER_GroupingFunction(void);
void TOWER_UnpackPIWO(void * frame_data);
void TOWER_DisplayPicture(void * windows_data);
void TOWER_SetLedColumn();
void TOWER_InitTimerLED(void);
void TOWER_InitDmaLED(void);
void TOWER_PinMux(void);

