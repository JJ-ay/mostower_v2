
#include "netif_tower.h"

void NETIF_Config(void * arg)
{
    /* Wait for the end of welcome message */
    ulTaskNotifyTake(pdTRUE,portMAX_DELAY);      
    xSemaphoreTake(mut_comm,portMAX_DELAY);

    static mdio_handle_t mdioHandle = {.ops = &enet_ops};
    static phy_handle_t phyHandle   = {.phyAddr = BOARD_ENET0_PHY_ADDRESS, .mdioHandle = &mdioHandle, .ops = &phyksz8081_ops};

    ip4_addr_t netif_ipaddr, netif_netmask, netif_gw, mcast_group;
    ethernetif_config_t enet_config = {
        .phyHandle  = &phyHandle,
        .macAddress = configMAC_ADDR,
    };

    mdioHandle.resource.csrClock_Hz = CLOCK_FREQ;

    /* A place for editing IP, NETMASK and GATEWAY */
    SYSMPU_Type *base = SYSMPU;
    IP4_ADDR(&netif_ipaddr, 192, 168, 69, 68);
    IP4_ADDR(&netif_netmask, 255, 255, 255, 0);
    IP4_ADDR(&netif_gw, 192, 168, 69, 1);

    base->CESR &= ~SYSMPU_CESR_VLD_MASK;

    tcpip_init(NULL, NULL);

    netifapi_netif_add(&netif, &netif_ipaddr, &netif_netmask, &netif_gw, &enet_config, ethernetif0_init, tcpip_input);

    /* Force IP filter off (the board naturally filters multicast IP)*/
    /* Note: This is super ugly and MUST NOT be used in bigger networks with unknown devices*/
    ENET->GALR = 1;
    ENET->GAUR = 1;

    /* Join multicast group */
    netif.flags |= NETIF_FLAG_IGMP;
    IP4_ADDR(&mcast_group, 239, 255, 43, 21);

    LOCK_TCPIP_CORE();      // The world burns without it...
    igmp_joingroup_netif(&netif, &mcast_group);
    UNLOCK_TCPIP_CORE();    // This too...

    netifapi_netif_set_default(&netif);
    netifapi_netif_set_up(&netif);

    xSemaphoreGive(mut_comm);
    xTaskNotifyGive(tsk_rcv_eth);

    vTaskDelete(NULL);
    TSK_DIE(); // infinite delay just in case
}

void NETIF_RcvHandler(void * frame_buffer)
{
    /* Wait for netif config to finish */
    ulTaskNotifyTake(pdTRUE,portMAX_DELAY);
    vTaskDelay(pdMS_TO_TICKS(100));         // let idle task clear memory after welcome and net config...
                                            // ...because we are starving idle completely afterwards
    xSemaphoreTake(mut_comm,portMAX_DELAY);

    struct netconn *conn;
    struct netbuf *buf;
    BaseType_t err_mut;
    err_t err;

    while((conn = netconn_new(NETCONN_UDP)) == NULL)
        vTaskDelay(pdMS_TO_TICKS(10));
    netconn_bind(conn, IP_ADDR_ANY, 6667);

    LED_GREEN_OFF();
    xTimerStart(tim_led_r,0);
    PRINTF("OK: Connection setup complete\r\n");

    xSemaphoreGive(mut_comm);

    while (1)
    {
        err_mut = xSemaphoreTake(mut_comm,pdMS_TO_TICKS(50));     // might want to trim the time a little
        if(err_mut == pdTRUE)
        {
            err = netconn_recv(conn, &buf);
            if (err == ERR_OK)
            {
                if(netbuf_copy(buf, frame_buffer, TOWER_BUF_LEN) != buf->p->tot_len)
                {
                    PRINTF("ERROR: Unable to read netbuf -- discarding frame\r\n");
                    netbuf_delete(buf);
                    xSemaphoreGive(mut_comm);
                }
                else
                {
                    frame_len = buf->p->tot_len;
                    netbuf_delete(buf);
                    xSemaphoreGive(mut_comm);
                    xTaskNotifyGive(tsk_handle_frame);
                }
            }
            else if((err != ERR_OK)&&(err != ERR_TIMEOUT))
            {
                PRINTF("ERROR: Data receiver error\r\n");
                xSemaphoreGive(mut_comm);
            }
        }
        else
        {
            PRINTF("ERROR: Receiver couldn't acquire mutex -- data loss possible\r\n");
        }
    }
}

void NETIF_HandleFrame(void * frame_data) // really need to change this shit -- a subject for future implemention of new frames
{
    while(1)
    {
        ulTaskNotifyTake(true,portMAX_DELAY);

        uint8_t frm_piwo[2] = {TOWER_LEN_COLUMNS,TOWER_LEN_ROWS};  

        if((!memcmp(frm_piwo,frame_data,2)) && (frame_len == TOWER_WINDOWS_TOTAL*3+2))  
        {
            PRINTF("OK: Frame received\r\n");
            xTaskNotifyGive(tsk_unpack_piwo);
        }
        else
            PRINTF("ERROR: Unknown/deformed frame received -- discarding frame\r\n");
    }
}
