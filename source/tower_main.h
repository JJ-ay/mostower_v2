
#pragma once

#include "FreeRTOS.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"

#define TOWER_LEN_COLUMNS 20U
#define TOWER_LEN_ROWS 30U
#define TOWER_WINDOWS_TOTAL (TOWER_LEN_ROWS*TOWER_LEN_COLUMNS)
#define TOWER_DISPLAY_WELCOME 1
#define TOWER_PORT_COUNT 4

#define PRIO_READ_ETH       1U
#define PRIO_CONF_NET       2U
#define PRIO_WELCOME        3U
#define PRIO_HANDLE_FRAME   4U
#define PRIO_UNPACK_FRAME   5U
#define PRIO_DISPL_PIC      6U

#define TIM_ID_LED_B        0U
#define TIM_ID_WELCOME      1U


// TASKS                            
TaskHandle_t tsk_rcv_eth;          
TaskHandle_t tsk_unpack_piwo;      
TaskHandle_t tsk_handle_frame;      
TaskHandle_t tsk_display_pic;       
TaskHandle_t tsk_welcome;        
TaskHandle_t tsk_config_net;        

// TIMERS                           
TimerHandle_t tim_led_r;            
TimerHandle_t tim_welcome;            

// SEMAPHORES
SemaphoreHandle_t mut_comm;    


void TSK_DIE(void);
