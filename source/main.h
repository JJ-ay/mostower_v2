
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "FreeRTOSConfig.h"
#include "lwipopts.h"

#include "tcpip.h"
#include "ethernet.h"
#include "enet_ethernetif.h"

#include "board.h"
#include "fsl_device_registers.h"
#include "pin_mux.h"
#include "clock_config.h"

#include "tcpip_priv.h"

#ifdef __cplusplus
}
#endif

#include "tower.h"
#include "netif_tower.h"
